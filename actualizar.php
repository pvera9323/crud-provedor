<?php 
    include("conexion.php");
    $con=conectar();

$id=$_GET['id'];

$sql="SELECT * FROM proveedor WHERE ID_PROVEEDOR='$id'";
$query=mysqli_query($con,$sql);

$row=mysqli_fetch_array($query);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
        <title>Actualizar</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        
    </head>
    <body>
    <body style=" background: url('imagen/ferreteria.jpg') no-repeat;  background-size:cover;">
                <div class="container mt-5">
                <h1>Actualice datos del provedor</h1>
                    <form action="update.php" method="POST">
                    
                                <input type="hidden" name="ID_PROVEEDOR" value="<?php echo $row['ID_PROVEEDOR']  ?>">
                                
                                <input type="text" class="form-control mb-3" name="NOM_PROVEEDOR" placeholder="Actualice su nombre" value="<?php echo $row['NOM_PROVEEDOR']  ?>">
                                <input type="text" class="form-control mb-3" name="DIR_PROVEEDOR" placeholder="Actualice dirección" value="<?php echo $row['DIR_PROVEEDOR']  ?>">
                                <input type="text" class="form-control mb-3" name="TEL_PROVEEDOR" placeholder="Actualice su telefono" value="<?php echo $row['TEL_PROVEEDOR']  ?>">
                                <input type="text" class="form-control mb-3" name="FECHA_ENTREGA" placeholder="Actualice fecha" value="<?php echo $row['FECHA_ENTREGA']  ?>">

                            <input type="submit" class="btn btn-primary btn-block" value="Actualizar">
                    </form>
                    
                </div>
    </body>
</html>