<?php 
    include("conexion.php");
    $con=conectar();

    $sql="SELECT *  FROM proveedor";
    $query=mysqli_query($con,$sql);

    $row=mysqli_fetch_array($query);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title> Provedores</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        
    </head>
    <body>
    <body style=" background: url('imagen/ferreteria.jpg') no-repeat;  background-size:cover;">
            <div class="container mt-5">
                    <div class="row"> 
                        
                        <div class="col-md-3">
                            <h1>Ingrese datos del provedor</h1>
                                <form action="insertar.php" method="POST">

                                    <input type="text" class="form-control mb-3" name="ID_PROVEEDOR" placeholder="Codigo del provedor">
                                    <input type="text" class="form-control mb-3" name="NOM_PROVEEDOR" placeholder="Nombre del provedor">
                                    <input type="text" class="form-control mb-3" name="DIR_PROVEEDOR" placeholder="Dirección">
                                    <input type="text" class="form-control mb-3" name="TEL_PROVEEDOR" placeholder="Telefono">
                                    <input type="text" class="form-control mb-3" name="FECHA_ENTREGA" placeholder="Fecha">
                                    <input type="submit" class="btn btn-primary">
                                </form>
                        </div>

                        <div class="col-md-8">
                            <table class="table" >
                                <thead class="table table-dark table-sm"" >
                                    <tr>
                                        <th>Codigo del provedor</th>
                                        <th>Nombre del provedor</th>
                                        <th>Dirección</th>
                                        <th>Telefono</th>
                                        <th>Fecha de entrega</th>
                                    </tr>
                                </thead>

                                <tbody>
                                        <?php
                                            while($row=mysqli_fetch_array($query)){
                                        ?>
                                            <tr>
                                                <th><?php  echo $row['ID_PROVEEDOR']?></th>
                                                <th><?php  echo $row['NOM_PROVEEDOR']?></th>
                                                <th><?php  echo $row['DIR_PROVEEDOR']?></th>
                                                <th><?php  echo $row['TEL_PROVEEDOR']?></th>
                                                <th><?php  echo $row['FECHA_ENTREGA']?></th>
                                                <th></th>
                                                <th><a href="actualizar.php?id=<?php echo $row['ID_PROVEEDOR'] ?>" class="btn btn-info">Actualizar</a></th>
                                                <th><a href="delete.php?id=<?php echo $row['ID_PROVEEDOR'] ?>" class="btn btn-danger">Eliminar</a></th>                                        
                                            </tr>
                                        <?php 
                                            }
                                        ?>
                                </tbody>
                            </table>
                        </div>
                    </div>  
            </div>
    </body>
</html>